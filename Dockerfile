FROM varnish:7.3

ARG VMOD_URL=
ARG VMOD_BUILD_DEPS=
ARG VMOD_RUN_DEPS=
ARG SKIP_CHECK=

USER root
RUN set -e;
RUN apt-get update; 
RUN apt-get -y install /pkgs/*.deb $VMOD_DEPS $VMOD_BUILD_DEPS $VMOD_RUN_DEPS git libpcre2-dev libedit-dev; 
# grab and compile the right commit of the Varnish source
RUN git clone https://github.com/varnishcache/varnish-cache.git /tmp/varnish-cache; 

WORKDIR /tmp/varnish-cache; 
# make sure to check out the exact same version that's already compiled and installed
RUN git checkout $(varnishd -V 2>&1 | grep -o '[0-9a-f]\{40\}*'); 
RUN ./autogen.sh; 
RUN ./configure --with-persistent-storage; 
RUN make -j 16; 
RUN export VARNISHSRC=/tmp/varnish-cache; 
# build and install
RUN install-vmod $VMOD_URL; 
RUN apt-get -y purge --auto-remove $VMOD_DEPS varnish-dev git libpcre2-dev libedit-dev; 
RUN rm -rf /var/lib/apt/lists/* /tmp/varnish-cache

USER varnish